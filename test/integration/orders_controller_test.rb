# test/integration/orders_controller_test.rb
require 'test_helper'
require 'sidekiq/testing'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @stock1 = 'AAPL'
    @stock2 = 'GOOGL'

    @sell_orders_stock1 = @sell_orders_stock2 = @buy_orders_stock1 = @buy_orders_stock2 = []
    10.times do 
      @sell_orders_stock1 << create_sell_orders(@stock1, rand(9))
      @buy_orders_stock1 << create_buy_orders(@stock1, rand(9))
      @sell_orders_stock2 << create_sell_orders(@stock2, rand(9))
      @buy_orders_stock2 << create_buy_orders(@stock2, rand(9))
    end
  end

  test "should get order with associated trades" do
    Sidekiq::Worker.clear_all
    [@sell_orders_stock1, @sell_orders_stock2, @buy_orders_stock1, @buy_orders_stock2].flatten.each do |order|
      get orders_url(id: order.id)
      assert_response :success

      puts order.inspect

      order_data = JSON.parse(response.body)
      assert_equal order.id, order_data['order']['id']
      assert_equal order.order_type, order_data['order']['order_type']
      assert_equal order.quantity, order_data['order']['quantity']
      assert_equal order.price, order_data['order']['price']
      assert_equal order.stock_name, order_data['order']['stock_name']

      puts order_data.inspect

      trades = order.trades
      assert_equal trades.length, order_data['trades'].length
      order_data['trades'].each do |trade_data|
        assert trades.any? { |trade| trade.id == trade_data['id'] }
      end
    end
  end

  private

  def create_sell_orders(stock_name, count)
    Sidekiq::Testing.inline! do
      orders = []
      count.times do
        orders << Order.create(order_type: 'sell', quantity: rand(100..150), price: rand(500..700), stock_name: stock_name, expiry_time: 300)
      end
    end
    orders
  end

  def create_buy_orders(stock_name, count)
    Sidekiq::Testing.inline! do
      orders = []
      count.times do
        orders << Order.create(order_type: 'buy', quantity: rand(100..140), price: rand(550..600), stock_name: stock_name, expiry_time: 300)
      end
    end
    orders
  end
end
