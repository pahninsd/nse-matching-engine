class CreateTrades < ActiveRecord::Migration[7.1]
  def change
    create_table :trades do |t|
      t.integer :buy_order_id
      t.integer :sell_order_id
      t.integer :quantity
      t.integer :price

      t.timestamps
    end
  end
end
