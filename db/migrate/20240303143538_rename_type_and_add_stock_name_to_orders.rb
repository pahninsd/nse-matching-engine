class RenameTypeAndAddStockNameToOrders < ActiveRecord::Migration[7.1]
  def change
    rename_column :orders, :type, :order_type
    add_column :orders, :stock_name, :string
  end
end
