class AddExpiryTimeToOrders < ActiveRecord::Migration[7.1]
  def change
    add_column :orders, :expiry_time, :integer, default: 0
  end
end
