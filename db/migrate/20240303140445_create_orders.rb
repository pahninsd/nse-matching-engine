class CreateOrders < ActiveRecord::Migration[7.1]
  def change
    create_table :orders do |t|
      t.string :type
      t.integer :quantity
      t.integer :price
      t.boolean :completed

      t.timestamps
    end
  end
end
