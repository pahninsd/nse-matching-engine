# app/jobs/populate_order_book_job.rb
class PopulateOrderBookJob < ApplicationJob
  queue_as :default

  def perform(order_id)
    order = Order.find(order_id)

    expiry = (Time.now + order.expiry_time.seconds ).to_i

    key = [ "order_book", order.stock_name.upcase, order.order_type ].join(":")

    $redis.zadd(key, expiry, order_id)
    $redis.hset("#{key}:#{order_id}", {
        order_id: order.id,
        quantity: order.quantity,
        price: order.price,
        expiry: expiry
      }
    )
    Sidekiq.logger << "\n\nhere\n\n"

    ProcessOrderJob.perform_later(order_id)
  end
end
