class ProcessOrderJob < ApplicationJob
  queue_as :default

  def perform(order_id)
    order = Order.find order_id
    matching_engine = MatchingEngine.new
    matching_engine.match_order(order)

    matching_engine.trades.each do |trade|
      Trade.create(trade)
      buy_order = Order.find(trade[:buy_order_id])
      sell_order = Order.find(trade[:sell_order_id])
      buy_order.update(completed: true) if buy_order.quantity == buy_order.traded_quantity
      sell_order.update(completed: true) if sell_order.quantity == sell_order.traded_quantity
    end
  end
end
