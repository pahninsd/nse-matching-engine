class MatchingEngine
  attr_reader :trades, :order_book
  
  def initialize
    @order_book = []
    @trades = []
  end

  def match_order(order)
    if order.order_type.to_sym == :buy
      match_for_buy_order(order)
    elsif order.order_type.to_sym == :sell
      match_for_sell_order(order)
    else
      raise ArgumentError, "Invalid order type"
    end
  end

  def match_for_buy_order(buy_order)
    fetch_matching_sell_orders(stock_name: buy_order.stock_name.upcase, price: buy_order.price)
    buy_quantity = buy_order.quantity

    puts "\n\n In match engine sell order"
    puts @order_book.inspect
    puts "\n\n"

    @order_book.each do |sell_order|
      if buy_quantity > 0 && sell_order[:quantity] > 0
        quantity = [buy_quantity, sell_order[:quantity]].min
        buy_quantity -= quantity
        sell_order[:quantity] -= quantity
        add_trade(sell_order[:order_id], buy_order.id, quantity, sell_order[:price])
      else
        break
      end
    end

    update_order_book([ "order_book", buy_order.stock_name.upcase, "sell" ].join(":"))
    update_current_order([ "order_book", buy_order.stock_name.upcase, "buy" ].join(":"), buy_order, buy_quantity)
  end

  def match_for_sell_order(sell_order)
    fetch_matching_buy_orders(stock_name: sell_order.stock_name.upcase, price: sell_order.price)
    sell_quantity = sell_order.quantity
    puts "\n\n In match engine sell order"
    puts @order_book.inspect
    puts "\n\n"
    @order_book.each do |buy_order|
      if sell_quantity > 0 && buy_order[:quantity] > 0
        quantity = [sell_quantity, buy_order[:quantity]].min
        sell_quantity -= quantity
        buy_order[:quantity] -= quantity
        add_trade(sell_order.id, buy_order[:order_id], quantity, buy_order[:price])
      else
        break
      end
    end

    update_order_book([ "order_book", sell_order.stock_name.upcase, "buy" ].join(":"))
    update_current_order([ "order_book", sell_order.stock_name.upcase, "sell" ].join(":"), sell_order, sell_quantity)
  end

  def add_trade(sell_order_id, buy_order_id, quantity, price)
    @trades << { buy_order_id: buy_order_id, sell_order_id: sell_order_id, quantity: quantity, price: price }
  end

  private

  def update_order_book(key)
    $redis.del(key)

    @order_book.each do |order|
      expiry = order[:expiry]
      # $redis.zadd(key, expiry, order.to_json) if order[:quantity] > 0
      if order[:quantity] > 0
        $redis.zadd(key, expiry, order[:order_id])
        $redis.hset("#{key}:#{order[:order_id]}", order)
      end
    end
  end

  def update_current_order(key, order, new_qty)
    if new_qty > 0
      $redis.hset("#{key}:#{order.id}", :quantity, new_qty)
    else
      $redis.del("#{key}:#{order.id}")
    end
  end

  def fetch_matching_sell_orders(stock_name: , price: )
    key = [ "order_book", stock_name.upcase, "sell" ].join(":")

    # fetch the sell orders that have not expired and filter by price and sort by price ascending
    @order_book = fetch_unexpired_orders(key).select { |e| e[:price] <= price }.sort_by { |e| e[:price] }

    @order_book
  end

  def fetch_matching_buy_orders(stock_name: , price: )
    key = [ "order_book", stock_name.upcase, "buy" ].join(":")

    # fetch the buy orders that have not expired and filter by price and sort by price descending
    @order_book = fetch_unexpired_orders(key).select { |e| puts e.inspect; e[:price] >= price }.sort_by { |e| -e[:price] }

    @order_book
  end

  def fetch_unexpired_orders(key)
    return_array = []

    $redis.zrange(key, Time.now.to_i, '+inf', byscore: true, withscores: true).map { |e|
      mapped_hash = HashWithIndifferentAccess.new($redis.hgetall("#{key}:#{e[0]}"))
      mapped_hash[:price] = mapped_hash[:price].to_i
      mapped_hash[:quantity] = mapped_hash[:quantity].to_i
      mapped_hash[:order_id] = mapped_hash[:order_id].to_i
      mapped_hash[:expiry] = mapped_hash[:expiry].to_i
      return_array << mapped_hash unless mapped_hash.empty?
      # JSON.parse(e[0], symbolize_names: true).merge!(expiry: e[1]) 
    }

    return_array
  end
end