class Order < ApplicationRecord
  has_many :buy_trades, foreign_key: :buy_order_id, class_name: "Trade"
  has_many :sell_trades, foreign_key: :sell_order_id, class_name: "Trade"

  after_create_commit :populate_order_book

  validates :order_type, presence: true, inclusion: { in: %w(buy sell), message: "%{value} is not a valid order type" }
  validates :quantity, presence: true, numericality: { greater_than: 0 }
  validates :price, presence: true, numericality: { greater_than: 0 }
  validates :stock_name, presence: true

  def populate_order_book
    PopulateOrderBookJob.perform_later(self.id)
  end

  def traded_quantity
    send("#{order_type}_trades").sum(:quantity)
  end

  def trades
    order_type == "sell" ? sell_trades : buy_trades 
  end
end
