# app/controllers/orders_controller.rb
class OrdersController < ApplicationController
  protect_from_forgery with: :null_session
  def create
    order = Order.new(order_params)
    if order.save
      render json: { message: 'Order created successfully', id: order.id }, status: :created
    else
      render json: { errors: order.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    order = Order.find_by(id: params[:id])
    if order
      render json: { order: order, trades: order.trades }
    else
      render json: { error: 'Order not found' }, status: :not_found
    end
  end

  private

  def order_params
    params.require(:order).permit(:order_type, :quantity, :price, :stock_name, :expiry_time)
  end
end
