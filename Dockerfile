# Use the official Ruby image from the Docker Hub
FROM ruby:3.0.3

# Set working directory
WORKDIR /app

# Install dependencies
RUN apt-get update -qq && \
    apt-get install -y nodejs && \
    apt-get install -y npm && \
    npm install -g yarn && \
    gem install bundler:2.5.6

# Set bundler environment variables
ENV BUNDLE_PATH /app/vendor/bundle
ENV BUNDLE_BIN /app/vendor/bundle/bin
ENV GEM_HOME /app/vendor/bundle
ENV PATH $BUNDLE_BIN:$PATH

# Copy Gemfile and Gemfile.lock to the Docker image and install gems
COPY Gemfile Gemfile.lock ./
RUN bundle install

# Copy the rest of the application code into the Docker image
COPY . .

# Expose port 3000 to the Docker host, so we can access the Rails server
EXPOSE 3000

# Start the Rails server
CMD ["/app/bin/rails", "server", "-b", "0.0.0.0"]