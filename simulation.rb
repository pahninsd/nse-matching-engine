require 'httparty'
require 'json'

# Define the base URL of your Rails application
BASE_URL = 'http://localhost:3000' # Adjust this URL as needed

# Method to send a POST request to create an order
$order_ids = []
def create_order(order_type, quantity, price, stock_name, expiry_time)
  url = "#{BASE_URL}/orders"
  options = {
    body: {
      order: {
        order_type: order_type,
        quantity: quantity,
        price: price,
        stock_name: stock_name,
        expiry_time: expiry_time
      }
    }
  }

  response = HTTParty.post(url, options)
  $order_ids << JSON.parse(response.body)["id"]
  puts "Response code: #{response.code} body: #{response.body}"
end

def get_order(id)
  url = "#{BASE_URL}/orders/#{id}"

  response = HTTParty.get(url)
  puts "Response code: #{response.code} \nbody:" 
  puts JSON.pretty_generate(JSON.parse(response.body))
end

# Simulate traffic by creating orders
def simulate_traffic
  # Simulate creation of sell orders for stock AAPL
  500.times do
    rand(5).times do
      create_order('sell', rand(100..150), rand(500..510), 'AAPL', rand(3600))
    end

    # Simulate creation of sell orders for stock GOOGL
    rand(5).times do
      create_order('sell', rand(100..150), rand(900..930), 'GOOGL', rand(3600))
    end

    # Simulate creation of buy orders for stock AAPL
    rand(5).times do
      create_order('buy', rand(100..140), rand(505..515), 'AAPL', rand(3600))
    end

    # Simulate creation of buy orders for stock GOOGL
    rand(5).times do
      create_order('buy', rand(100..140), rand(890..905), 'GOOGL', rand(3600))
    end
  end

  $order_ids.each do |id|
    get_order(id)
  end
end

# Execute the simulation
simulate_traffic
